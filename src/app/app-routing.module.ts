import { SavedStudentComponent } from './saved-student/saved-student.component';
import { StudentsComponent } from './students/students.component';
import { UsersComponent } from './users/users.component';
import { NetworkFormComponent } from './network-form/network-form.component';
import { LambdaComponent } from './lambda/lambda.component';
import { CustomersComponent } from './customers/customers.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { PostsComponent } from './posts/posts.component';
import { SavedComponent } from './saved/saved.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { CityFormComponent } from './city-form/city-form.component';
import { ClassifyComponent } from './classify/classify.component';
import { StudentFormComponent } from './student-form/student-form.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'customers', component: CustomersComponent },
  { path: 'customerform', component: CustomerFormComponent },
  { path: 'lambda', component: LambdaComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'saved', component: SavedComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'city', component: CityFormComponent },
  { path: 'classify/:network', component: ClassifyComponent },
  { path: 'network', component: NetworkFormComponent },
  { path: 'users', component: UsersComponent },
  { path: 'students', component: StudentsComponent },
  { path: 'studentform', component: StudentFormComponent },
  { path: 'studentssaved', component: SavedStudentComponent },
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
