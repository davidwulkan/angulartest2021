import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { WelcomeComponent } from './welcome/welcome.component';
import { MatCardModule } from '@angular/material/card';
import { CustomersComponent } from './customers/customers.component';
import { AngularFirestoreModule} from '@angular/fire/firestore';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LambdaComponent } from './lambda/lambda.component';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { PostsComponent } from './posts/posts.component';
import { SavedComponent } from './saved/saved.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { CityFormComponent } from './city-form/city-form.component';
import { MatSelectModule } from '@angular/material/select';
import { ClassifyComponent } from './classify/classify.component';
import { MatRadioModule } from '@angular/material/radio';
import { NetworkFormComponent } from './network-form/network-form.component';
import { UsersComponent } from './users/users.component';
import { StudentsComponent } from './students/students.component';
import { StudentFormComponent } from './student-form/student-form.component';
import { SavedStudentComponent } from './saved-student/saved-student.component';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    RegisterComponent,
    LoginComponent,
    WelcomeComponent,
    CustomersComponent,
    CustomerFormComponent,
    LambdaComponent,
    PostsComponent,
    SavedComponent,
    TemperaturesComponent,
    CityFormComponent,
    ClassifyComponent,
    NetworkFormComponent,
    UsersComponent,
    StudentsComponent,
    StudentFormComponent,
    SavedStudentComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSliderModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    MatInputModule,
    FormsModule,
    MatCardModule,
    AngularFirestoreModule,
    MatExpansionModule,
    MatPaginatorModule,
    HttpClientModule,
    MatTableModule,
    MatSelectModule,
    MatRadioModule,
    
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
