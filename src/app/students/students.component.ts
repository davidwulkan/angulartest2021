import { PredictionService } from './../prediction.service';
import { Student } from './../interfaces/student';
import { Component, OnInit, ViewChild } from '@angular/core';

import { StudentsService } from '../students.service';
import { MatPaginator } from '@angular/material/paginator';
import { AuthService } from '../auth.service';

@Component({
  selector: 'students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  students:Student[];  
  students$;
  userId:string;
  email:string;
  editstate = [];
  addStudentFormOpen = false;
  firstDocumentArrived: any; //Save first document in snapshot of items received
  lastDocumentArrived: any; //Save last document in snapshot of items received
  @ViewChild(MatPaginator) paginator: MatPaginator;
  prev_strt_at:  any[] = []; //Keep the array of first document of previous pages

  prediction='unknown';
  saved:string;
  

  panelOpenState = false;
  constructor(private studentsService:StudentsService, public authService:AuthService, private PredictionService:PredictionService) { }

  deleteStudent(id:string){
    this.studentsService.deleteStudent(this.userId, id);
  }

  updateStudent(student:Student){
    this.studentsService.updateStudent(this.userId, student.id, student.name, student.math, student.psych, student.sahar);
  }

  add(student:Student){
    this.studentsService.addStudent(this.userId, student.name, student.math, student.psych, student.sahar);
  }

  predict(math,psych, sahar){
    console.log(math,psych, sahar);
    this.PredictionService.prediction(math,psych, sahar).subscribe(
      res => {
        console.log(res);
        console.log(typeof res);       
      
      if(res>0.5){
        this.prediction='Continue the school'
      }else{
        this.prediction='Dropout of the school'
      }return this.prediction;
     }
    )

  }

  
  addPrediction(userId,id,prediction){
    console.log(id);
    console.log(userId);
    console.log(prediction);
    this.studentsService.addPredict(userId,id,prediction)
    this.saved = "Prediction saved at the DB!";
  }

    
  push_prev_startAt(prev_first_doc) {
     this.prev_strt_at.push(prev_first_doc);
  }
  
  remove_last_from_start_at(){
    this.prev_strt_at.splice(this.prev_strt_at.length-1, 1);
  }
  
  get_prev_startAt(){
      return this.prev_strt_at[this.prev_strt_at.length - 1];
  }

  nextPage(){
    this.students$ = this.studentsService.nextPage(this.userId, this.lastDocumentArrived);
    this.students$.subscribe(
      docs =>{
        this.lastDocumentArrived = docs[docs.length-1].payload.doc;
        this.firstDocumentArrived = docs[0].payload.doc;
        this.push_prev_startAt(this.firstDocumentArrived);

        this.students = [];
        for(let document of docs){
          const student:Student = document.payload.doc.data();
          student.id = document.payload.doc.id;
          this.students.push(student);
        }
      }
    )    
  }

  prevPage(){
    this.remove_last_from_start_at()
    this.students$ = this.studentsService.prevPage(this.userId,this.get_prev_startAt());
    this.students$.subscribe(docs => {   
      this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
      this.firstDocumentArrived = docs[0].payload.doc;
      this.students = [];
        for (let document of docs) {
          const student:Student = document.payload.doc.data();
          student.id = document.payload.doc.id;
          this.students.push(student);
      }
    });
  }

  
  ngOnInit(): void {
    
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.email = user.email;
        console.log(this.userId);
        this.students$ = this.studentsService.getStudents(this.userId);
        
        this.students$.subscribe(
          docs =>{
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            console.log('init worked');
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.firstDocumentArrived = docs[0].payload.doc;
            this.push_prev_startAt(this.firstDocumentArrived);  

            this.students = [];            
            for(let document of docs){
              const student:Student = document.payload.doc.data();
              student.id = document.payload.doc.id;
              this.students.push(student);              
            }
          }
        )
      }
    )
    
  }
  

}