import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {

  private url = " https://qee2u1w5ye.execute-api.us-east-1.amazonaws.com/test";

  prediction(math:number, psych:number, sahar:boolean){
    let json = {
      "data": 
        {
          "math": math,
          "psych": psych,
          "sahar": sahar,
        }
    }
    let body  = JSON.stringify(json);
    console.log("testing");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }
  constructor(private http:HttpClient) { }
}
