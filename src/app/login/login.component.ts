import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;

  onSubmit(){
    this.auth.login(this.email,this.password);
    
  }

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
  }

}