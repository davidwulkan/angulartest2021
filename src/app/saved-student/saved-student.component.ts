import { SavedCustomerService } from './../saved-customer.service';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-saved-student',
  templateUrl: './saved-student.component.html',
  styleUrls: ['./saved-student.component.css']
})
export class SavedStudentComponent implements OnInit {

  constructor(private db:AngularFirestore, public authService:AuthService, private SavedCustomerService:SavedCustomerService) { }

  userId:string;
  students$;
  
  

  

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.students$ = this.SavedCustomerService.getSaved(this.userId);
      }
    )
  }
}
