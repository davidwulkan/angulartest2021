import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SavedCustomerService {

  studentCollection:AngularFirestoreCollection;
    public getSaved(userId){
      this.studentCollection = this.db.collection(`users/${userId}/students`);
      return this.studentCollection.snapshotChanges().pipe(map( 
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        )
      ))
    } 

    


  

  constructor(private db:AngularFirestore) { }
}