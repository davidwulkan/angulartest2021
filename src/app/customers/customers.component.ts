import { PredictService } from './../predict.service';
import { CustomersService } from './../customers.service';
import { Customer } from './../interfaces/customer';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { AuthService } from '../auth.service';
import { stringify } from '@angular/compiler/src/util';


@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers:Customer[];  
  customers$;
  userId:string;
  editstate = [];
  addCustomerFormOpen = false;
  firstDocumentArrived: any; //Save first document in snapshot of items received
  lastDocumentArrived: any; //Save last document in snapshot of items received
  @ViewChild(MatPaginator) paginator: MatPaginator;
  prev_strt_at:  any[] = []; //Keep the array of first document of previous pages

  prediction='unknown';
  saved:string;
  

  panelOpenState = false;
  constructor(private customersService:CustomersService, public authService:AuthService, private PredictService:PredictService) { }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId, id);
  }

  updateCustomer(customer:Customer){
    this.customersService.updateCustomer(this.userId, customer.id, customer.name, customer.income, customer.years);
  }

  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.name, customer.income, customer.years);
  }

  predict(years,income){
    console.log(years,income);
    this.PredictService.prediction(years,income).subscribe(
      res => {
        console.log(res);
        console.log(typeof res);       
      
      if(res>0.5){
        this.prediction='Will return'
      }else{
        this.prediction='Will not return'
      }return this.prediction;
     }
    )

  }

  
  addPrediction(userId,id,prediction){
    console.log(id);
    console.log(userId);
    console.log(prediction);
    this.customersService.addPredict(userId,id,prediction)
    this.saved = "Prediction Saved!";
  }

    
  push_prev_startAt(prev_first_doc) {
     this.prev_strt_at.push(prev_first_doc);
  }
  
  remove_last_from_start_at(){
    this.prev_strt_at.splice(this.prev_strt_at.length-1, 1);
  }
  
  get_prev_startAt(){
      return this.prev_strt_at[this.prev_strt_at.length - 1];
  }

  nextPage(){
    this.customers$ = this.customersService.nextPage(this.userId, this.lastDocumentArrived);
    this.customers$.subscribe(
      docs =>{
        this.lastDocumentArrived = docs[docs.length-1].payload.doc;
        this.firstDocumentArrived = docs[0].payload.doc;
        this.push_prev_startAt(this.firstDocumentArrived);

        this.customers = [];
        for(let document of docs){
          const customer:Customer = document.payload.doc.data();
          customer.id = document.payload.doc.id;
          this.customers.push(customer);
        }
      }
    )    
  }

  prevPage(){
    this.remove_last_from_start_at()
    this.customers$ = this.customersService.prevPage(this.userId,this.get_prev_startAt());
    this.customers$.subscribe(docs => {   
      this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
      this.firstDocumentArrived = docs[0].payload.doc;
      this.customers = [];
        for (let document of docs) {
          const customer:Customer = document.payload.doc.data();
          customer.id = document.payload.doc.id;
          this.customers.push(customer);
      }
    });
  }

  
  ngOnInit(): void {
    
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.customers$ = this.customersService.getCustomers(this.userId);
        
        this.customers$.subscribe(
          docs =>{
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            console.log('init worked');
            this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            this.firstDocumentArrived = docs[0].payload.doc;
            this.push_prev_startAt(this.firstDocumentArrived);  

            this.customers = [];            
            for(let document of docs){
              const customer:Customer = document.payload.doc.data();
              customer.id = document.payload.doc.id;
              this.customers.push(customer);              
            }
          }
        )
      }
    )
    
  }
  

}