import { Student } from './../interfaces/student';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';



@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  @Input() name:string;
  @Input() math:number;
  @Input() psych:number;
  @Input() sahar:boolean;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();
  

  sahars:Object[] = [{id:1, name:'true'},{id:2, name:'false'}]
  

  onSubmit(){
    
  }


  updateParent(){
    let student:Student = {id:this.id, name:this.name, math:this.math, psych:this.psych, sahar:this.sahar};
    this.update.emit(student);
    if(this.formType == "Add Student"){
      this.name = null;
      this.math = null;
      this.psych = null;
      this.sahar = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
