import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

studentsCollection:AngularFirestoreCollection;
userCollection:AngularFirestoreCollection = this.db.collection('users');

public getStudents(userId){
  this.studentsCollection = this.db.collection(`users/${userId}/students`,
  ref => ref.orderBy('name', 'asc').limit(4));
  return this.studentsCollection.snapshotChanges()
}
  
nextPage(userId,startAfter): Observable<any[]>{
  this.studentsCollection = this.db.collection(`users/${userId}/students`, 
  ref => ref.limit(4).orderBy('name', 'asc').startAfter(startAfter))    
  return this.studentsCollection.snapshotChanges();
}
    
prevPage(userId,startAt): Observable<any[]>{
  this.studentsCollection = this.db.collection(`users/${userId}/students`, 
  ref => ref.limit(4).orderBy('name', 'asc').startAt(startAt))    
  return this.studentsCollection.snapshotChanges();
}
    

deleteStudent(Userid:string, id:string){
  this.db.doc(`users/${Userid}/students/${id}`).delete();
}

addStudent(userId:string, name:string, math:number, psych:number, sahar:boolean){
  const student = {name:name, math:math, psych:psych, sahar:sahar };
  this.userCollection.doc(userId).collection('students').add(student);
}

updateStudent(userId:string, id:string, name:string, math:number, psych:number, sahar:boolean){
  this.db.doc(`users/${userId}/students/${id}`).update(
  {
    name:name,
    math:math,
    psych:psych,
    sahar:sahar
  }
 );
}

public addPredict(userId:string,id:string,prediction:string){
  this.db.doc(`users/${userId}/students/${id}`).update(
    {
      prediction:prediction
    }
  )
}

addSaved(userId:string,name:string){
  const student = {name:name}; 
  this.userCollection.doc(userId).collection('students').add(student);
  console.log('student:', student)
}
  
    

constructor(private db:AngularFirestore) { }

}
